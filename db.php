<?php

function getDb() {
    $config = require(__DIR__ . '/config.php');

    $pdo = new PDO(
        "mysql:host=".$config['db']['host'].';port='.$config['db']['port'].';dbname='.$config['db']['dbname'],
        $config['db']['user'],
        $config['db']['pass']
    );

    return $pdo;
}