<?php

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if($uri === '/')
{
    require '../routes/home.php';
    die();
} else if ($uri === '/api/places') {
    require '../routes/api.php';
    die();
}

header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    