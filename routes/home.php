<!DOCTYPE html>
<html>
    <head>
        <title>Red Rowant - Test task</title>
        <link href="styles.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/gh/xcash/bootstrap-autocomplete@v2.3.5/dist/latest/bootstrap-autocomplete.min.js"></script>
        <script src="autocomplete.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="search text-center">
            <h1 class="h3 mb-3 font-weight-normal">Search Address:</h1>
            <input
                placeholder="Enter address"
                class="form-control autoComplete"
                type="text"
                autocomplete="off"
                onclick="$('.autoComplete').autoComplete('show');"
            />
        </div>
    </body>
</html>