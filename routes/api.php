<?php

require('../db.php');

header('Content-Type: application/json');

$queryString = $_GET['q'];
$db = getDb();

if (!$queryString || !$db) {
    echo json_encode([]);
    die();
}

$statement = $db->prepare('SELECT search_results FROM places_search WHERE search = ? AND created_at > ? ORDER BY created_at DESC LIMIT 1');
;

if ($statement->execute([$queryString, time() - 60 * 60 * 24 * 7])) {
    $results =  $statement->fetchAll(PDO::FETCH_COLUMN, 0);
    if (count($results)) {
        echo $results[0];
        die();
    }
}

$endpoint = 'http://ahunter.ru/site/suggest/address';
$params = http_build_query([
    'output' => 'json',
    'query' => $queryString
]);
$url = $endpoint . '?' . $params;

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $url);
$apiResults = curl_exec($ch);
curl_close($ch);

$apiResults = array_map(function ($item) {
    return $item->value;
}, json_decode($apiResults)->suggestions);

if (count($apiResults) > 6) {
    $apiResults = array_slice($apiResults, 0, 6);
}

$apiResults = json_encode($apiResults);

$db->prepare('INSERT INTO places_search (created_at, search, search_results) VALUES (?,?,?)')
    ->execute([time(), $queryString, $apiResults]);

echo $apiResults;
